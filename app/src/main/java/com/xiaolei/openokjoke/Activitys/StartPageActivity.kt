package com.xiaolei.openokjoke.Activitys

import android.animation.ValueAnimator
import android.os.Bundle
import android.view.animation.LinearInterpolator
import android.widget.FrameLayout
import com.xiaolei.openokjoke.Base.BaseActivity
import com.xiaolei.openokjoke.Exts.onAnimEnd
import com.xiaolei.openokjoke.R
import com.xiaolei.openokjoke.Utils.StateBarUtil
import kotlinx.android.synthetic.main.activity_start_page.*

/**
 * 启动页
 * Created by xiaolei on 2018/3/9.
 */
class StartPageActivity : BaseActivity()
{
    private val maxValue = 3

    override fun onCreate(savedInstanceState: Bundle?)
    {
        setContentView(R.layout.activity_start_page)
        super.onCreate(savedInstanceState)
    }

    override fun initObj()
    {

    }

    override fun initView()
    {
        val param = click_jump.layoutParams as FrameLayout.LayoutParams
        param.topMargin += StateBarUtil.getStateBarHeigh(this)
        click_jump.requestLayout()
    }

    override fun initData()
    {

    }

    override fun setListener()
    {
        click_jump.setOnClickListener {
            nextPage()
        }
    }

    override fun loadData()
    {
        val animator = ValueAnimator.ofInt(maxValue, 0)
        animator.duration = maxValue * 1000L
        animator.interpolator = LinearInterpolator()
        animator.addUpdateListener { animation ->
            val value = animation.animatedValue as Int
            click_jump.text = "跳过 ${value}s"
        }
        animator.onAnimEnd {
            nextPage()
        }
        animator.start()
    }

    private fun nextPage()
    {
        if (!this.isFinishing)
        {
            startActivity(MainActivity::class.java)
//            startActivity(TestADActivity::class.java)
            finish()
        }
    }
}