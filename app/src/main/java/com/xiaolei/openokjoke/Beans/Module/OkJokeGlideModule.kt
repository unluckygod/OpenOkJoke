package com.xiaolei.openokjoke.Beans.Module

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

/**
 * Created by xiaolei on 2018/3/9.
 */
@GlideModule
class OkJokeGlideModule : AppGlideModule()