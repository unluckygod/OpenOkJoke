package com.xiaolei.openokjoke.Beans

/**
 * Created by xiaolei on 2018/3/14.
 */
class QQLuckyBean(val id: Int?
                  , val qq: String?
                  , val score: String?
                  , val grade: String?
                  , val analysis: String?
                  , val desc: String)